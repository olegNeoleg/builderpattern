import Foundation

enum Color: String {
    case brightYellow, purple, teal, bleachedPorple, burgundy, pastelGreen, chocolateBrown, pink, maroon, orange
}

enum Shape: String {
    case sphere, cube, pyramid, prism, cone, cylinder
}

class FigureBuilder {
    private var figure = Figure()
    
    func withColors(_ colors: [Color]) -> FigureBuilder {
        figure.color = colors
        return self
    }
    
    func withShape(_ shape: Shape) -> FigureBuilder {
        figure.shape = shape
        return self
    }
    
    func withSize(_ size: Int) -> FigureBuilder {
        figure.size = size
        return self
    }
    
    func reset() {
        figure = Figure()
    }
    
    func build() -> Figure {
        return figure
    }
}

class Figure {
    var shape: Shape = .cube
    var color: [Color] = []
    var size = 1
    
    func description() -> String {
        return "Shape: \(shape.rawValue) | Colors: \(color.map({ $0.rawValue }).joined(separator: ", ")) | Size: \(size)"
    }
}

let figureBuilder = FigureBuilder().withColors([.bleachedPorple]).withShape(.cone).withSize(1)

let figure = figureBuilder.build()
debugPrint(figure.description())
