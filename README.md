# README #

Some samples with builder pattern

### Notes ###

The builder pattern works best for creating complex objects that require multiple inputs using a series of steps.
If your object doesn’t have several inputs or can’t be created step by step, the builder pattern may be more trouble than it’s worth.
Instead, consider providing convenience initializers to create the object.

### Links ###

Builder doc:  
https://refactoring.guru/ru/design-patterns/builder/swift/example  
https://medium.com/swift2go/builder-pattern-in-swift-for-beginners-79415d30872e  

Function chaining:  
https://abhimuralidharan.medium.com/chaining-methods-in-swift-not-optional-chaining-3007d1714985  