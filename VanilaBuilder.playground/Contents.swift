import Foundation

protocol Builder {
    func addColors(_ colors: [Color])
    func setShape(_ shape: Shape)
    func setSize(_ size: Int)
}

class FigureBuilder: Builder {
    
    private(set) var figure = Figure()
    
    func addColors(_ colors: [Color]) {
        figure.color.append(contentsOf: colors)
    }
    
    func setShape(_ shape: Shape) {
        figure.shape = shape
    }
    
    func setSize(_ size: Int) {
        figure.size = size
    }
    
    func reset() {
        figure = Figure()
    }
    
    func build() -> Figure {
        return figure
    }
}

enum Color: String {
    case brightYellow, purple, teal, bleachedPorple, burgundy, pastelGreen, chocolateBrown, pink, maroon, orange
}

enum Shape: String {
    case sphere, cube, pyramid, prism, cone, cylinder
}

class Figure {
    var shape: Shape = .cube
    var color: [Color] = []
    var size = 1
    
    func description() -> String {
        return "Shape: \(shape.rawValue) | Colors: \(color.map({ $0.rawValue }).joined(separator: ", ")) | Size: \(size)"
    }
}

class ConcreteDirector {
    private var builder: Builder?

    func update(builder: Builder) {
        self.builder = builder
    }

    func createPurpleTealPinkCylinder() {
        builder?.addColors([.purple, .teal, .pink])
        builder?.setShape(.cylinder)
        builder?.setSize(1)
    }

    func createChocolateBrownPyramid() {
        builder?.addColors([.chocolateBrown])
        builder?.setShape(.pyramid)
        builder?.setSize(1)
    }
}

// MARK: Predefined director usage
//
let concreteBuilder = FigureBuilder()
let concreteCirector = ConcreteDirector()
concreteCirector.update(builder: concreteBuilder)
concreteCirector.createChocolateBrownPyramid()

debugPrint(concreteBuilder.build().description())


// MARK: Custom builder usage
//
let customBuilder = FigureBuilder()
customBuilder.addColors([.bleachedPorple, .burgundy])
customBuilder.setShape(.sphere)
customBuilder.setSize(1)

debugPrint(customBuilder.build().description())
